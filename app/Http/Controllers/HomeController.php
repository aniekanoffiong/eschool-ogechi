<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\classesModel;
use App\StudentModel;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   //get the admin class details
        // $adminClass = classesModel::whereAdmin(Auth::user()->id)->first();
       //Also get the students data
    //     $students = StudentModel::whereAdmin(Auth::user()->id)->get();
    //   return view('home',['Classname'=> $adminClass->name,'ClassType'=>$adminClass->classType,'students'=>$students]);
        return view('home');
    }

}
