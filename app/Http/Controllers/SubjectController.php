<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\SubjectModel;
use App\SubjectType;
class SubjectController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = SubjectModel::whereAdmin(Auth::user()->id)->get();
        return view('/subject/index',['subjects'=>$subjects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/subject/addSubject');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'subjectName'=>'required|min:2|max:40'
        ]);

    $subject = SubjectModel::insert(array('admin'=>Auth::user()->id,'name'=>$request->get('subjectName')));
            if ($subject) {
                return redirect('/subject/addSubject')->with(['subject_success'=>'Subject successfully added']);
            }else{
                return redirect('/subject/addSubject')->with(['subject_error'=>'Opps, something went wrong']);
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editSubject = SubjectModel::where([['admin',Auth::user()->id],['id',$id],])->first();
        return view('/subject/editSubject',['editSubject'=>$editSubject]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request,[
          'subjectName'=>'required|min:2|max:40'
        ]);
       $subject = SubjectModel::where([['admin',Auth::user()->id],['id',$id],])->update(array('name'=>$request->get('subjectName')));
       if ($subject) {
           return redirect('/subject/editSubject/'.$id)->with(['subject_success'=>'Subject successfully updated']);
       }else{
          return redirect('/subject/editSubject/'.$id)->with(['subject_error'=>'Opps, something went wrong']);
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
