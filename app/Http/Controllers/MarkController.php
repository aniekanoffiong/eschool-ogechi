<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\MarkModel;
use App\StudentModel;
use App\SubjectModel;
use App\SubjectType;

class MarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //display your student with a button to add mark
        $students = StudentModel::whereAdmin(Auth::user()->id)->get();
        return view('/mark/index',['students'=>$students]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        //return view('/mark/addMark');
        //get the subject, subject type and student name and picture
        $student = StudentModel::select('name','student_pic')->whereId($id)->first();
        $subjects = SubjectModel::whereAdmin(Auth::user()->id)->get();
        $subject_type = SubjectType::all();
        return view('/mark/addMark',['stdId'=>$id,'subjects'=>$subjects,'subject_type'=>$subject_type,'student'=>$student]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $saveMark = MarkModel::insert(array('admin'=>Auth::user()->id,'subject'=>$request->get('subject'),'subjectType'=>$request->get('subject_type'),'mark'=>$request->get('mark'),'term'=>$request->get('term')));
        if ($saveMark) {
            return redirect()->back()->with(['successMsg'=>'Data successfully added']);
        }else{
            return redirect()->back()->with(['errorMsg'=>'Opps, something went wrong']);
        }
    }

    public function markList($id){
        //get the student picture,name and also the student's subject and term
        $student = StudentModel::select('name','student_pic')->whereId($id)->first();
        $mark = MarkModel::whereAdmin(Auth::user()->id)->get();
        foreach ($mark as $value) {
          $subject = SubjectModel::select('name')->whereId($value->subject)->get();
        }
        //get the subject, subject type
        //

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
