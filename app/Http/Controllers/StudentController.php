<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\StudentModel;
class StudentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //show all my student
        $students = StudentModel::whereAdmin(Auth::user()->id)->get();
        return view('/student/index',['students'=>$students]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/student/addStudent');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
          'stdName'=>'required|min:3|max:30',
          'dob'=>'required',
          'gender'=>'required',
          'address'=>'required|min:2|max:200'
        ]);

        $file = $request->file('stdPic');
        $destinationPath='/public/studentPic/';
        $file->move(base_path().$destinationPath,$file->getClientOriginalName());
        $student_pic = $file->getClientOriginalName();
        //saving student data
        $student = StudentModel::insert(array('admin'=> Auth::user()->id,'name'=> $request->get('stdName'),
            'dob'=>$request->get('dob'),'student_pic'=>$student_pic,'gender'=>$request->get('gender'),
            'address'=>$request->get('address'),'parent_id'=>'0'));
        if ($student) {
            return redirect('/student/addStudent')->with(['std_success'=>'Data successfully add']);
        }else{
            return redirect('/student/addStudent')->with(['std_error'=>'Opps, something went wrong']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
