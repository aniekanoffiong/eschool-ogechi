@extends('layouts.e_layout')
@section('slider')
<!-- banner-text -->
		<div class="slider">
			<div class="callbacks_container">
				<ul class="rslides callbacks callbacks1" id="slider4">
					<li>
						<div class="w3layouts-banner-top">
							<div class="container">
								<div class="agileits-banner-info">
									<h3>School<span>Management</span>System</h3>
										<p> For Better Interaction  </p>
									<div class="agileits_w3layouts_more menu__item">
				<a href="#" class="menu__link" data-toggle="modal" data-target="#myModal">Learn More</a>
			</div>
								</div>	
							</div>
						</div>
					</li>
					<li>
						<div class="w3layouts-banner-top w3layouts-banner-top1">
							<div class="container">
								<div class="agileits-banner-info">
									<h3>School<span>Management</span>System</h3>
									 <p>FOR BETTER INTERACTION</p>
									<div class="agileits_w3layouts_more menu__item">
				<a href="#" class="menu__link" data-toggle="modal" data-target="#myModal">Learn More</a>
			</div>
								</div>	
							</div>
						</div>
					</li>
					<li>
						<div class="w3layouts-banner-top w3layouts-banner-top2">
							<div class="container">
								<div class="agileits-banner-info">
									<h3>School<span>Management</span>System</h3>
									<p>FOR BETTER INTERACTION</p>
									<div class="agileits_w3layouts_more menu__item">
											<a href="#" class="menu__link" data-toggle="modal" data-target="#myModal">Learn More</a>
										</div>
								</div>
								
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="clearfix"> </div>
			
			<!--banner Slider starts Here-->
		</div>
		<div class="thim-click-to-bottom">
				<a href="#about" class="scroll">
					<i class="fa fa-long-arrow-down" aria-hidden="true"></i>
				</a>
			</div>
@endsection 
@section('navbar')
<!-- header -->
		<div class="header-w3layouts"> 
			<!-- Navigation -->
			<nav class="navbar navbar-default navbar-fixed-top">
				<div class="container">
					<div class="navbar-header page-scroll">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
							<span class="sr-only">E-SCHOOL</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<h1><a class="navbar-brand" href="index.html">E-SCHOOL</a></h1>
					</div> 
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<ul class="nav navbar-nav navbar-right">
							<!-- Hidden li included to remove active class from about link when scrolled up past about section -->
							<li class="hidden"><a class="page-scroll" href="#page-top"></a>	</li>
							<li><a class="page-scroll scroll" href="#home">Home</a></li>
							<li><a class="page-scroll scroll" href="#about">About</a></li>
							<li><a class="page-scroll scroll" href="#services">Services</a></li>
							<li><a class="page-scroll scroll" href="#plans">Login/Sign Up</a></li>
							<li><a class="page-scroll scroll" href="#contact">Contact</a></li>
						</ul>
					</div>
					<!-- /.navbar-collapse -->
				</div>
				<!-- /.container -->
			</nav>  
		</div>	
		<!-- //header -->
@endsection
@section('content')

	</div>	
	<!-- //banner --> 

<!-- about -->
<div class="about" id="about">
      <div class="col-md-6 ab-w3-agile-info">
	   <div class="ab-w3-agile-info-text">
	     <h2 class="title-w3">About Us</h2>
		 <p class="sub-text one">E-School Management System</p>
		 <p>This is a web-based School Management application. It will design for better interaction between students, teachers, parents & management. The main purpose of building this application is, the parents of students are very busy now days, so they can’t monitoring their children and them activities a properly and regularly. This E-School management system helps the parents monitor their children from anywhere. They can check their children’s academic performance from a remote location.</p>
			<div class="agileits_w3layouts_more menu__item one">
				<a href="#" class="menu__link" data-toggle="modal" data-target="#myModal">Learn More</a>
			</div>
		  </div>
		  <div class="ab-w3-agile-inner">
	       <div class="col-md-6 ab-w3-agile-part">
				<h4>Dancing Supplies</h4>
		     <p>Lorem ipsum dolor sit amet, consectetur adipisc elit. Proin ultricies vestibulum velit. a galley of type and scrambled it to make a type specimen book. Proin ultricies vestibulum velit.</p>
	       </div>
			<div class="col-md-6 ab-w3-agile-part two">
			<h4>Master Classes</h4>
		     <p>Lorem ipsum dolor sit amet, consectetur adipisc elit. Proin ultricies vestibulum velit., a galley of type and scrambled it to make a type specimen book. Proin ultricies vestibulum velit.</p>
	       </div>
			<div class="clearfix"></div>		   
	  </div>	
   </div>	 	  
	  <div class="col-md-6 ab-w3-agile-img">
	     
	  </div>
 
		<div class="clearfix"></div>
</div>
	<!-- //about -->
											<!-- Modal1 -->
													<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
														<div class="modal-dialog">
														<!-- Modal content-->
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal">&times;</button>
																	<img src="{{url('images/2.jpg')}}" alt=" " class="img-responsive">
																	<h5>E-School Management System</h5>
																	<p>
																	E-School Management System is a web-based School Management application. Design for better interaction between students, teachers, parents & management.</p>
																</div>
															</div>
														</div>
													</div>
													<!-- //Modal1 -->

		<!-- /classes-->
	<div class="services" id="classes">
		<div class="container">

		 <h3 class="title-w3">Our Services</h3><br>
		 <p class="sub-text"> We help parent monitor their children’s academic<br> performance from a remote location.</p>
             <div class="wthree-agile-classes-section">
                    <div class="col-md-4 class-grid">
                        <div class="class-grid-img hvr-sweep-to-top">
                           <img src="{{ url('images/pl.jpg')}}" width="" alt=""/>
                            <div class="caption">
                                <h5> Student Attendance</h5>
                            </div>
                        </div>
                    
                        <h4>Attendance</h4>
                    
                    </div>
                <div class="col-md-4 class-grid">
                        <div class="class-grid-img hvr-sweep-to-top">
                           <img src="{{ url('images/child_3.jpg')}}" width="" alt=""/>
                            <div class="caption">
                                <h5> Student Assignment</h5>
                            </div>
                        </div>
                    
                        <h4>assignment</h4>
                    
                    </div>
                <div class="col-md-4 class-grid">
                        <div class="class-grid-img hvr-sweep-to-top">
                           <img src="{{ url('images/p.jpg')}}" width="" alt=""/>
                            <div class="caption">
                                <h5> Student Test & Exam </h5>
                            </div>
                        </div>
                    
                        <h4>test & Exam</h4>
                    
                    </div>
                </div>
                <div class="clearfix"> </div>
             </div>

         </div>
        
   <!-- //classes-->
   <div class="main" id="plans">
		<div id="particles-js"></div>
			<div class="priceing-table-main">
		       <h3 class="title-w3 three">Login/Sign Up</h3><br><br>
		    	<div class="col-md-6 price-grid ">
					<div class="price-block agile">
						<div class="price-gd-top pric-clr2">
							<h4>E-School Management System</h4>
							<h3><span>Admin Login</span></h3>
							<h5>New User?<br><a href="#register here">Register Here</a></h5>
						</div>
						<div class="price-gd-bottom">
							<div class="price-list">
								
									<ul>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									
								</ul>
								<h6 class="bed two"><i class="fa fa-street-view" aria-hidden="true"></i></h6>
								
							</div>
							<div class="price-selet pric-sclr2">
								<h3><a href="login">Login</a></h3>

							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 price-grid lost">
					<div class="price-block agile">
						<div class="price-gd-top pric-clr3">
							<h4>E-School Management System</h4>
							<h3><span>Student Login</span></h3>
							<h5>New User?<br><a href="#register here">Register Here</a></h5>
						</div>
						<div class="price-gd-bottom">
							<div class="price-list">
								<ul>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									<li><i class="fa fa-star" aria-hidden="true"></i></li>
									
								</ul>
								<h6 class="bed three"><i class="fa fa-street-view" aria-hidden="true"></i></h6>
							</div>
							<div class="price-selet pric-sclr3">
								<h3><a class="book popup-with-zoom-anim button-isi zoomIn animated" data-wow-delay=".5s" href="#small-dialog">Login</a></h3>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		
		</div>	
	</div>	
	<!--//prices -->
				<!-- Modal2-->
								<div class="pop-up"> 
			<div id="small-dialog" class="mfp-hide book-form">
				<h4>Register Here</h4>
				<form action="#" method="post">
				Student Name:<input type="text" name="Name" placeholder="Your Name" required=""/><br>
				Date of Birth:<input type="text" name="Name" placeholder="Your Name" required=""/><br>
				Father Name:<input type="text" name="Name" placeholder="Your Name" required=""/><br>
				Mother Name:<input type="text" name="Name" placeholder="Your Name" required=""/><br>
				Password:<input type="password" name="Name" placeholder="password" required=""/><br>
				Re-type Password:<input type="password" name="Name" placeholder="password" required=""/><br>
				Gender:	<form>
  						<input type="radio" name="sex" value="male" checked>Male
						<br>
						<input type="radio" name="sex" value="female">Female
						</form>
				Profile picture:		
				Phone:<input type="text" name="Name" placeholder="Your Name" required=""/><br>
				Address:<input type="text" name="Name" placeholder="Your Name" required=""/><br>
				         <input type="submit" value="Submit"> 
						
					</div>
					


				</form>
			</div>
		</div>
		
						<!-- //Modal2 -->		
	
	<!-- //gallery -->
	 <!-- //classes-->
   <div class="testmonials" id="monials">
		<div id="particles-js1"></div>
		   <div class="client-top">
		  <h3 class="title-w3 three">Hear What Parent & Student have to Say</h3>
		    <div class="slider">
					<div class="callbacks_container">
						<ul class="rslides" id="slider3">
							<li>
								 <div class="agileits-clients">
									
									<div class=" client_agile_info">
									   
											<div class="c-img"><i class="fa fa-quote-right"></i> </div>
											<p>Determination today leads  to success tommorrow.<br> Believe you can and you surely will. be the <span>BEST</span> of your kind </p>
											<h4><img src="{{ url('images/oge_1.jpg')}}" alt="">Ogechi Okoro</h4>
										 
									</div>
									
								</div>
							</li>
							<li>
							<div class="agileits-clients">
								
								<div class="client_agile_info">
								   
                                        <div class="c-img"><i class="fa fa-quote-right"></i> </div>
										<p>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>
										<h4><img src="{{ url('images/child_4.jpg')}}" alt=""> Blessing Ezinne</h4>
									 
								</div>
								<div class="clearfix"></div>
								</div>
							</li>
							<li>
							<div class="agileits-clients">
							     <div class=" client_agile_info">
								   
                                        <div class="c-img"><i class="fa fa-quote-right"></i> </div>
										<p>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>
										<h4><img src="{{ url('images/oge_2.jpg')}}" alt="">Rosemary Joseph</h4>
									 
								</div>
								
								</div>
							</li>
						</ul>
					</div>
				</div>

				</div>
	</div>	
	<!--//prices -->

	<!-- Contact -->
	<div class="agile-contact">

		<div class="col-md-6 contact-map-right">
			<div id="map"></div>
		</div>
		<div class="col-md-6 left-contact">
		<div class="cont-top">
			<h5><i class="fa fa-envelope" aria-hidden="true"></i>Email</h5>
			<a href="mailto:info@example.com">oogehchris@gmail.com</a>
		</div>
		<div class="con-bot">
			<div class="left-bw3">
				<h5><i class="fa fa-map-marker" aria-hidden="true"></i>Address</h5>
				<p>Start Innovation Hub</p>
				<p>IBB Avenue uyo,</p>
				<p>Nigeria, Africa.</p>
			</div>
			<div class="right-bw3">
				<h5><i class="fa fa-phone" aria-hidden="true"></i>Phone</h5>
				<p>Telephone : +234 (080) 30553991</p>
				<p>+234 85467</p>
				<p>+234 85467</p>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>

	<div class="contact" id="contact">
	<div id="particles-js2"></div>
		<div class="contact-top">
		

			<h3 class="title-w3 con">Contact Us</h3>

			<form action="#" method="post" class="contact_form slideanim">

				<div class="message">
					<div class="col-md-6 col-sm-6 grid_6 c1">
						<input type="text" class="margin-right" Name="Name" placeholder="Name" required="">
						<input type="email" Name="Email" placeholder="Email" required="">
						<input type="text" class="margin-right" Name="Phone Number" placeholder="Phone Number" required="">
					</div>

					<div class="col-md-6 col-sm-6 grid_6 c1">
						<textarea name="Message" placeholder="Message" required=""></textarea>
					</div>
					<div class="clearfix"></div>
				</div>

				<input type="submit" value="SEND MESSAGE">
			</form>
				<section class="social">
                        <ul>
							<li><a class="icon fb" href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a class="icon tw" href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a class="icon gp" href="#"><i class="fa fa-google-plus"></i></a></li>
						</ul>
				</section>
		</div>
	</div>
	<!-- //Contact -->
@endsection