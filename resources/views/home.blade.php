@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Profile section
                      @if($errors->any() > 0)
                      <div class="alert alert-warning">
                       @foreach ($errors->all() as $error)
                         <strong>$error</strong><br>
                       @endforeach
                    </div>
                    @endif
                  @if(Session::has('Adminsuccess_msg'))
                    <div class="alert alert-success">
                       {{Session::get('Adminsuccess_msg')}}
                    </div>
                  @elseif(Session::has('Adminerror_msg'))
                    <div class="alert alert-danger">
                       {{Session::get('Adminerror_msg')}}  
                    </div>
                  @endif
                </div>

                <div class="panel-body">
                    <div class="row">
                      <div class="col-md-12" style="padding-left: 30px;padding-right: 30px">
                        @if(empty(Auth::user()->profile_pic))
                         <img src="{{url('images/oge_1.jpg')}}" width="100%" class="img-responsive img-circle"><br>
                         @else
                          <img src="{{url('adminPic/'.Auth::user()->profile_pic)}}" width="100%" class="img-responsive img-circle"><br>
                         @endif
                     </div>
                     <div id="profilebtn" class="col-md-12"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

          <div class="panel-body">
            <div id="content"></div>
                <script type="text/babel">
                  var ProfileComponent = React.createClass({
                    getInitialState: function(){
                       return{profile: true};
                    },
                    editProfile: function(){
                      this.setState({profile:false})
                    },
                    saveProfile: function(){
                      this.setState({profile: true})
                    },
                    cancelProfile: function(){
                      this.setState({profile: true})
                    },
                    showButton: function(){
                         return(<div><center><button onClick={this.editProfile} className="btn btn-primary">Edit profile</button></center></div>);
                    },
                    showForm: function(){
                      return(
                        <div>
                          <div class="col-md-12 alert alert-success">
                           <strong>
                             NAME: {{Auth::user()->name}}<br/>
                             @if(!empty(Auth::user()->phone))
                             PHONE: {{Auth::user()->phone}}<br/>
                             @endif
                             <hr/>
                             <div className="form-group col-md-6">
                              <div className='col-md-12'>
                                <a href="{{url('/admin/edit/'.Auth::user()->id)}}"  className="btn btn-success">Edit</a>
                              </div>
                          </div>
                          <div className="form-group col-md-6">
                              <div className='col-md-12'><button onClick={this.cancelProfile} className="btn btn-warning">Close</button></div>
                          </div>
                           </strong>
                          </div>
                        </div>
                      );
                    },
                    render: function(){
                       if (this.state.profile) {
                           return this.showButton();
                       }else{
                          return this.showForm();
                       }
                    }
                  });

                  var ContentComponent = React.createClass({
                    getInitialState: function(){
                      return{attendance: true}
                    },
                    setAttendance: function(){
                      this.setState({attendance: false})
                    },
                    addAttendance: function(){
                      return(
                        <div>
                           @if(empty($students))
                              <div className="alert alert-warning">
                                 <strong>No student in the system.</strong>
                              </div>
                           @else
                             <form action="{{url('/attendance')}}" method="post" class="col-md-12">
                               <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                              <table className="table col-md-12">
                               <thead>
                                  <tr>
                                    <th>Name</th>
                                    <th>Gender</th>
                                    <th>Status</th>
                                  </tr>
                               </thead>
                               
                               <tbody>
                                   @foreach($students as $student)
                                      <tr>
                                        <td>{{$student->name}}</td>
                                        <td>{{$student->gender}}</td>
                                        <input type="hidden" name="stdName" value="{{$student->name}}" />
                                        <td><input type="checkbox" className="form-control" name="present_{{$student->name}}" /></td>
                                      </tr>
                                   @endforeach
                                </tbody>
                               <div className="col-md-3">
                                    <button className="btn btn-warning" onClick={this.returnContent}>Cancel</button>
                               </div>
                               <div className="col-md-3">
                                    <button type="submit" className="btn btn-success">Save</button>
                               </div>
                          </table>
                          </form>
                           @endif
                        </div>
                      );
                    },

                    returnContent: function(){
                      this.setState({attendance:true})
                    },
                    normalContent: function(){
                      return (
                        <div>
                            <div className="alert alert-success">
                               <strong>
                                  Welcome to Eschool management system, am Ogechi, Your Instructor and i will be your guild to
                                  help you make the best use of the system.
                               </strong>
                            </div>
                            <div className="col-md-3">
                              <button className="btn btn-primary" onClick={this.setAttendance}>Attendance entry</button>
                            </div>
                            <div className="col-md-3">
                              <a href="{{url('/mark/index')}}" className="btn btn-primary">Mark Maintenance</a>
                            </div>
                            <div className="col-md-3">
                              <a href="{{url('/student/addStudent')}}" className="btn btn-primary"><strong>+ Students</strong></a>
                            </div>
                            <div className="col-md-3">
                              <a href="" className="btn btn-warning"><strong>- Students</strong></a>
                            </div>
                        </div>
                      );
                    },
                      render: function() {
                        if(this.state.attendance) {
                           return this.normalContent();
                        }else{
                          return this.addAttendance();
                        }
                      }
                  });
                  ReactDOM.render(<ProfileComponent />, document.getElementById('profilebtn'));
                  ReactDOM.render(<ContentComponent />, document.getElementById('content'));
              </script>
         </div>
            </div>
        </div>
    </div>
</div>
@endsection
