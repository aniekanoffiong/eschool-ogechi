@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Profile section
                </div>

                <div class="panel-body">
                    <div class="row">
                      <div class="col-md-12" style="padding-left: 30px;padding-right: 30px">
                        @if(empty(Auth::user()->profile_pic))
                         <img src="{{url('images/oge_1.jpg')}}" width="100%" class="img-responsive img-circle"><br>
                         @else
                          <img src="{{url('adminPic/'.Auth::user()->profile_pic)}}" width="100%" class="img-responsive img-circle"><br>
                         @endif
                     </div>
                     
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                	@if(Session::has('successMsg'))
                       <div class="alert alert-success">
                       	 <strong>{{Session::get('successMsg')}}</strong>
                       </div>
                	@elseif(Session::has('errorMsg'))
                        <div class="alert alert-danger">
                       	 <strong>{{Session::get('errorMsg')}}</strong>
                       </div>
                	@endif
                	<div class="col-md-8">
                		<div class="col-md-3">
                		  <img src="{{url('studentPic/'.$student->student_pic)}}" class="img-responsive img-thumbnail" width="100%">
                	    </div>
	                   <div class="col-md-5">
	                  	&nbsp;<strong>{{$student->name}}</strong>
	                   </div>
                	</div>
                  @if(empty($subjects))
                      <a href="{{url('/subject/addSubject')}}" class="btn btn-primary"><b>+ Subject</b></a>
                  @endif
              </div>
             <div class="panel-body">
             	<hr>
             	<form method="post" action="{{url('/mark/addMark/'.$stdId)}}">
                	{{csrf_field() }}
                	<div class="form-group col-md-12">
                		<label><strong>Select subject</strong></label>
                		<select class="form-control" name="subject">
                			@foreach($subjects as $subject)
                              <option value="{{$subject->id}}">{{$subject->name}}</option>
                			@endforeach
                		</select>
                	</div>
                	<div class="form-group col-md-12">
                		<label><strong>Select subject type</strong></label>
                		<select class="form-control" name="subject_type">
                			@foreach($subject_type as $type)
                              <option value="{{$type->id}}">{{$type->subjectType}}</option>
                			@endforeach
                		</select>
                	</div>
                	<div class="form-group col-md-4">
                		<input type="number" class="form-control" name="mark" placeholder="mark">
                	</div>
                	<div class="form-group col-md-4">
                		<select class="form-control" name="term" required="">
                			 <option value=" ">Select term</option>
                			  <option value="first_term">First Term</option>
                              <option value="second_term">Second Term</option>
                			  <option value="third_term">Third Term</option>
                		</select>
                	</div>
                	<button type="submit" class="col-md-4 btn btn-primary">Submit</button>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
