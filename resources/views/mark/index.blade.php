@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Profile section
                </div>

                <div class="panel-body">
                    <div class="row">
                      <div class="col-md-12" style="padding-left: 30px;padding-right: 30px">
                        @if(empty(Auth::user()->profile_pic))
                         <img src="{{url('images/oge_1.jpg')}}" width="100%" class="img-responsive img-circle"><br>
                         @else
                          <img src="{{url('adminPic/'.Auth::user()->profile_pic)}}" width="100%" class="img-responsive img-circle"><br>
                         @endif
                     </div>
                     
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Dashboard
              </div>
             <div class="panel-body">
                @if(empty($students))
                   <div class="alert alert-danger">
                 	<strong>You have no student, click the button to add <a href="{{url('/student/addStudent')}}" class="btn btn-primary"><b>+ Student</b></a></strong>
                 </div>
                @else
                  <table class="table">
                      <thead> 
                        <tr> 
                          <th>Name</th>
                          <th>Gender</th> 
                          <th></th>
                        </tr> 
                      </thead> 
                      <tbody> 
                      	@foreach($students as $student)
                         <tr>
                           <td class="col-md-3">{{$student->name}}</td>
                           <td class="col-md-3">{{$student->gender}}</td>
                           <td class="col-md-3"><a href="{{url('/mark/addMark/'.$student->id)}}" class="btn btn-primary btn-sm">Add Mark</a></td>
                           <td class="col-md-3"><a href="{{url('/mark/markList/'.$student->id)}}" class="btn btn-warning btn-sm">View Mark List</a></td>
                         </tr>
                        @endforeach
                     </tbody>
                   </table>
                @endif
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
