@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Profile section
                </div>

                <div class="panel-body">
                    <div class="row">
                      <div class="col-md-12" style="padding-left: 30px;padding-right: 30px">
                        @if(empty(Auth::user()->profile_pic))
                         <img src="{{url('images/oge_1.jpg')}}" width="100%" class="img-responsive img-circle"><br>
                         @else
                          <img src="{{url('adminPic/'.Auth::user()->profile_pic)}}" width="100%" class="img-responsive img-circle"><br>
                         @endif
                     </div>
                     
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Dashboard
                  @if(Session::has('subject_success'))
                         <div class="alert alert-success">
                             <strong>{{Session::get('subject_success')}}</strong>
                         </div>
                  @elseif(Session::has('subject_error'))
                         <div class="alert alert-danger">
                             <strong>{{Session::get('subject_error')}}</strong>
                         </div>
                  @endif
              </div>
             <div class="panel-body">
              <form action="{{url('/subject/addSubject')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
              	 <div class="form-group col-md-12">
              	 	<input type="text" class="form-control" name="subjectName" placeholder="Enter subject name" required="">
              	 </div>
                 <div class="col-md-2">
                     <a href="{{url('/home')}}" class="btn btn-warning">Cancel</a>
                 </div>
                 <div class="col-md-2">
                     <button type="submit" class="btn btn-primary">Submit</button>
                 </div>
              </form>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
