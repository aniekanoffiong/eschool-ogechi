@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Profile section
                </div>

                <div class="panel-body">
                    <div class="row">
                      <div class="col-md-12" style="padding-left: 30px;padding-right: 30px">
                        @if(empty(Auth::user()->profile_pic))
                         <img src="{{url('images/oge_1.jpg')}}" width="100%" class="img-responsive img-circle"><br>
                         @else
                          <img src="{{url('adminPic/'.Auth::user()->profile_pic)}}" width="100%" class="img-responsive img-circle"><br>
                         @endif
                     </div>
                     
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Dashboard
                  @if(!empty($subjects))
                     <a href="{{url('/subject/addSubject')}}" class="btn btn-primary"><b>+ Subject</b></a>
                  @endif
              </div>
             <div class="panel-body">
              @if(empty($subjects))
                 <div class="alert alert-danger">
                 	<strong>You have no subject, click the button to add <a href="{{url('/subject/addSubject')}}" class="btn btn-primary"><b>+ Subject</b></a></strong>
                 </div>
              @else 
                <ol>
                @foreach($subjects as $subject)
                      <li><strong>{{$subject->name}}</strong> <a href="{{url('/subject/editSubject/'.$subject->id)}}" class="btn btn-warning btn-sm">Edit Subject</a></li><hr>   
                 @endforeach
                 </ol>
              @endif
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
