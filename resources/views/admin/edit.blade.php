@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Profile section
                </div>

                <div class="panel-body">
                    <div class="row">
                      <div class="col-md-12" style="padding-left: 30px;padding-right: 30px">
                        @if(empty(Auth::user()->profile_pic))
                         <img src="{{url('images/oge_1.jpg')}}" width="100%" class="img-responsive img-circle"><br>
                         @else
                          <img src="{{url('adminPic/'.Auth::user()->profile_pic)}}" width="100%" class="img-responsive img-circle"><br>
                         @endif
                     </div>
                     
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Dashboard
                   @if($errors->any() > 0)
                     <div class="alert alert-warning">
                  @foreach ($errors->all() as $error)
                     <strong>$error</strong><br>
                  @endforeach
                    </div>
                    @endif
                  @if(Session::has('success_msg'))
                    <div class="alert alert-success">
                       {{Session::get('success_msg')}}
                    </div>
                  @elseif(Session::has('error_msg'))
                    <div class="alert alert-danger">
                       {{Session::get('error_msg')}}  
                    </div>
                  @endif
              </div>
             <div class="panel-body">
            <div id="content">
              <form method="post" action="{{url('/admin/accountUpdate/'.Auth::user()->id)}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group col-md-6">
                  <input type="text" class="form-control" name="name" value="{{Auth::user()->name}}">
                </div>
                <div class="form-group col-md-6">
                  @if(empty(Auth::user()->phone))
                      <input type="tel" class="form-control" name="phone_number" placeholder="Enter your phone number">
                  @else
                    <input type="tel" class="form-control" name="phone_number" value="{{Auth::user()->phone}}">
                  @endif
                </div>
                <div class="form-group col-md-4">
                 <select className="form-control" name="className">
                    @if(empty($admin->name))
                       <option value="none">Class</option>
                    @else
                       <option value="{{$admin->name}}">{{$admin->name}}</option>
                    @endif
                    <option value="Jss1">Jss-1</option>
                    <option value="Jss2">Jss-2</option>
                    <option value="Jss3">Jss-3</option>
                    <option value="Sss1">Sss-1</option>
                    <option value="Sss2">Sss-2</option>
                    <option value="Sss3">Sss-3</option>
                 </select>
                </div>
                 <div class="form-group col-md-4">
                   <select className="form-control" name="classType">
                       @if(empty($admin->classType))
                       <option value="none">Type</option>
                       @else
                           <option value="{{$admin->classType}}">{{$admin->classType}}</option>
                       @endif
                      <option value="A">A</option>
                      <option value="B">B</option>
                      <option value="C">C</option>
                      <option value="D">D</option>
                      <option value="E">E</option>
                   </select>
                 </div>
                 <div class="form-group col-md-4">
                   <input type="file" name="picture">
                 </div>
                 <div class="col-md-4">
                   <a href="{{url('/home')}}" class="btn btn-warning">Cancel</a>
                 </div>
                 <div class="col-md-4">
                   <input type="submit" class="btn btn-primary" value="Submit">
                 </div>
              </form>
            </div>
         </div>
            </div>
        </div>
    </div>
</div>
@endsection
