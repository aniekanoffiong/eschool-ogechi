<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/index', function(){
   return view('index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('adminDetails','AdminController@store');
Route::get('/admin/edit/{id}','AdminController@edit');
Route::post('/admin/accountUpdate/{id}','AdminController@update');
Route::get('/student/index','StudentController@index');
Route::get('/student/addStudent','StudentController@create');
Route::post('/addStudent','StudentController@store');
Route::post('/attendance','AttendanceController@store');
Route::get('/subject/index','SubjectController@index');
Route::get('/subject/addSubject','SubjectController@create');
Route::post('/subject/addSubject','SubjectController@store');
Route::get('/subject/editSubject/{id}','SubjectController@edit');
Route::post('/updateSubject/{id}','SubjectController@update');
Route::get('/mark/index','MarkController@index');
Route::get('/mark/addMark/{id}','MarkController@create');
Route::post('/mark/addMark/{id}','MarkController@store');
Route::get('/mark/markList/{id}','MarkController@markList');
