<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassesModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classes_models', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin')->unsigned();
            $table->foreign('admin')->references('id')->on('users');
            $table->string('name');
            $table->string('classType');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classes_models');
    }
}
